import Vue from 'vue'
import VueResource from 'vue-resource'
import { API_RANKING_URL } from '../../router/paths'

Vue.use(VueResource)

const getRanking = () => {
  return Vue.http({
    url: API_RANKING_URL,
    method: 'GET'
  })
}

const getRankingByGame = (slug) => {
  return Vue.http({
    url: `${API_RANKING_URL}/game/${slug}`,
    method: 'GET'
  })
}


export default {
  getRanking,
  getRankingByGame,
}
