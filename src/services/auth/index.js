import Vue from 'vue'
import VueResource from 'vue-resource'
import store from '../../vuex/store'
import notificationService from '../../services/notification'
import { API_LOGIN_URL, API_USER_URL } from '../../router/paths'
import { router } from '../../router/config'

Vue.use(VueResource)

export default {
  user: {
    authenticated: false
  },

  login (body) {
    Vue.http({
      url: API_LOGIN_URL,
      method: 'POST',
      body: body
    }).then(response => {
      let token = response.data.token
      if (token !== undefined) {
        window.localStorage.setItem('id_token', token)
        store.dispatch('SET_TOKEN', token)
        /**
         * Get user profile
         */
        this.getUserProfile().then(response => {
          let user = response.json()
          this.user.authenticated = true
          store.dispatch('SET_USER', user)
          notificationService.show(notificationService.USER_LOGGED_IN)
          router.go('/')
        })
      }
    }, error => {
      window.console.log(error)
    })
  },

  logout () {
    window.localStorage.removeItem('id_token')
    store.dispatch('REMOVE_TOKEN')
    store.dispatch('REMOVE_USER')
    this.user.authenticated = false
  },

  checkAuth () {
    let jwt = window.localStorage.getItem('id_token')
    let user = store.state.user
    if (jwt) {
      this.user.authenticated = true
    } else {
      this.logout()
    }

    if (jwt && !user) {
      this.getUserProfile().then(response => {
        let user = response.json()
        store.dispatch('SET_USER', user)
      }, error => {
        window.console.log(error)
        this.logout()
      })
    }
  },

  getAuthHeader () {
    return {
      'Authorization': 'Bearer ' + window.localStorage.getItem('id_token'),
      'x-access-token': window.localStorage.getItem('id_token')
    }
  },
  /**
   * Se o login for bem sucedido obtém-se o profile do usuário logado
   * e retorna a Promise com o resultado
   */
  getUserProfile () {
    return Vue.http({
      url: API_USER_URL,
      method: 'GET',
      headers: this.getAuthHeader()
    })
  },

  updateUser (id, body) {
    return Vue.http({
      url: `${API_USER_URL}/${id}`,
      method: 'PUT',
      body: body
    })
  },

  getUser () {
    return store.state.auth.user
  },

  isLoggedIn () {
    return store.state.auth.authenticated
  }
}
