import Vue from 'vue'
import VueResource from 'vue-resource'
import { API_GAMES_URL } from '../../router/paths'

Vue.use(VueResource)

const getAll = () => {
  return Vue.http({
    url: API_GAMES_URL,
    method: 'GET'
  })
}

const getGameBySlug = (slug) => {
  return Vue.http({
    url: `${API_GAMES_URL}/slug/${slug}`,
    method: 'GET'
  })
}

export default {
  getAll,
  getGameBySlug
}
