import Vue from 'vue'
import VueResource from 'vue-resource'
import { API_UNIVERSITIES_URL } from '../../router/paths'

Vue.use(VueResource)

const getAll = () => {
  return Vue.http({
    url: API_UNIVERSITIES_URL,
    method: 'GET'
  })
}

export default {
  getAll
}
