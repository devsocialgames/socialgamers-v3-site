import Vue from 'vue'
import VueResource from 'vue-resource'
import { API_TOURNAMENTS_URL } from '../../router/paths'

Vue.use(VueResource)

const getTournaments = () => {
  return Vue.http({
    url: API_TOURNAMENTS_URL,
    method: 'GET'
  })
}

const getTournamentsByGame = (slug) => {
  return Vue.http({
    url: `${API_TOURNAMENTS_URL}/game/${slug}`,
    method: 'GET'
  })
}

const getTournament = (id) => {
  return Vue.http({
    url: `${API_TOURNAMENTS_URL}/${id}`,
    method: 'GET'
  })
}

export default {
  getTournaments,
  getTournamentsByGame,
  getTournament
}
