import Vue from 'vue'
import VueResource from 'vue-resource'
import { API_OFFERS_URL } from '../../router/paths'

Vue.use(VueResource)

const getAll = () => {
  return Vue.http({
    url: API_OFFERS_URL,
    method: 'GET'
  })
}

const save = (body) => {
  return Vue.http({
    url: API_OFFERS_URL,
    method: 'POST',
    body: body
  })
}

export default {
  getAll,
  save
}
