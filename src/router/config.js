
/**
 *
 * Router configuration
 * Set routes and components
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '../vuex/store'
import { sync } from 'vuex-router-sync'

Vue.use(VueRouter)

export const router = new VueRouter({
  hashbang: false,
  history: true,
  saveScrollPosition: true
})

router.map(routes)

router.redirect({
  '*': '/'
})

sync(store, router)
