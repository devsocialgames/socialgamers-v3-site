
/**
 *
 * Routes components
 */

import Home from '../modules/home/Index.vue'
import Contact from '../modules/contact/Index.vue'
import News from '../modules/news/Index.vue'
import Games from '../modules/tournaments/Games.vue'
import Tournaments from '../modules/tournaments/Index.vue'
import TournamentDetail from '../modules/tournaments/Detail.vue'
import Ranking from '../modules/ranking/Index.vue'
import Former from '../modules/former/Index.vue'
import Account from '../modules/account/Index.vue'
import Login from '../modules/auth/Login.vue'
import Signup from '../modules/auth/Signup.vue'
import Forgot from '../modules/auth/Forgot.vue'

const routes = {
  '/': {
    name: 'home',
    component: Home
  },
  '/news': {
    name: 'news',
    component: News
  },
  '/tournaments/game/:slug': {
    component: Tournaments,
    name: 'tournamentsByGame'
  },
  '/tournaments/:id': {
    component: TournamentDetail,
    name: 'tournament'
  },
  '/games': {
    component: Games,
    name: 'games'
  },
  '/ranking/game/:slug': {
    component: Ranking,
    name: 'Ranking'
  },

  '/contact': {
    name: 'contact',
    component: Contact
  },
  '/former': {
    name: 'former',
    component: Former
  },
  '/account': {
    name: 'account',
    component: Account
  },
  '/login': {
    name: 'login',
    component: Login
  },
  '/signup': {
    name: 'signup',
    component: Signup
  },
  '/forgot': {
    name: 'forgot',
    component: Forgot
  }
}

export default routes
