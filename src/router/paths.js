
/**
 *
 * API Routes Urls
 */

export const API_BASE_URL = 'http://api.socialgamers.com.br/api'
// export const API_BASE_URL = 'http://localhost:3500/api'
export const API_AUTH_URL = API_BASE_URL + '/auth'
export const API_USER_URL = API_AUTH_URL + '/user'
export const API_LOGIN_URL = API_AUTH_URL + '/login'
export const API_REGISTER_URL = API_AUTH_URL + '/register'
export const API_LOGOUT_URL = API_AUTH_URL + '/logout'
export const API_TOURNAMENTS_URL = API_BASE_URL + '/tournaments'
export const API_RANKING_URL = API_BASE_URL + '/ranking'
export const API_GAMES_URL = API_BASE_URL + '/games'
export const API_OFFERS_URL = API_BASE_URL + '/offers'
export const API_UNIVERSITIES_URL = API_BASE_URL + '/universities'

/**
 * Blog API
 */
export const API_BLOG_URL = 'http://blog.socialgamers.com.br'
export const API_BLOG_POSTS = API_BLOG_URL + '?json=get_recent_posts'
