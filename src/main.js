const moment = require('moment')
require('moment/locale/pt-br')

import Vue from 'vue'
import VueResource from 'vue-resource'
import VueMoment from 'vue-moment'
import VueValidator from 'vue-validator'
import App from './modules/App.vue'

import { router } from './router/config'

Vue.use(VueResource)
Vue.use(VueValidator)
Vue.use(VueMoment, {
  moment
})

router.start(App, '#app')
