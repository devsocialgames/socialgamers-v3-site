const RECEIVE_UNIVERSITIES = 'RECEIVE_UNIVERSITIES'
const REMOVE_UNIVERSITIES = 'REMOVE_UNIVERSITIES'

const state = {
  all: {}
}

const mutations = {
  [RECEIVE_UNIVERSITIES] (state, universities) {
    state.all = universities
  },

  [REMOVE_UNIVERSITIES] (state) {
    state.all = {}
  }
}

export default {
  state,
  mutations
}
