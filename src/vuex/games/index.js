const RECEIVE_GAMES = 'RECEIVE_GAMES'
const REMOVE_GAMES = 'REMOVE_GAMES'

const state = {
  all: {}
}

const mutations = {
  [RECEIVE_GAMES] (state, games) {
    state.all = games
  },

  [REMOVE_GAMES] (state) {
    state.all = {}
  }
}

export default {
  state,
  mutations
}
