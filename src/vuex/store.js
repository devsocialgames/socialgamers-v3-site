import Vue from 'vue'
import Vuex from 'vuex'
import games from './games'
import universities from './universities'
import notification from './notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    games,
    universities,
    notification
  }
})
