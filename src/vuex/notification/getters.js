const show = function (state) {
  return state.notification.show
}

const options = function (state) {
  return state.notification.options
}

export default {
  show,
  options
}
