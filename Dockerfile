# Set ubuntu image
FROM ubuntu

# File Author / Maintainer
MAINTAINER Donald Silveira <donaldsilveira@gmail.com>

RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get install -y libssl-dev && \
    apt-get install -y curl && \
    apt-get install -y git && \
    apt-get install -y wget

# Install NVM and Node
RUN curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh && \
    apt-get install -y nodejs

# Install PM2
RUN npm install -g pm2

RUN mkdir -p /var/www/

# Define working directory
WORKDIR /var/www/
ADD . /var/www/

# Expose port
EXPOSE 8080

# Run app
CMD pm2 start server.js --name sg-beta --no-daemon
