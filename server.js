const path = require('path')
const express = require('express')

const app = express()

app.set('port', 8080)

app.use(express.static(path.join(__dirname, './')))
app.use(express.static(path.join(__dirname, 'dist')))
app.use(express.static(path.join(__dirname, 'static')))

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'))
})

const server = app.listen(app.get('port'), () => {
  let port = server.address().port
  console.log('Magic happens on port ' + port)
})
