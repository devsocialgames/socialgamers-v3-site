(function ($) {
  $(document).ready(function () {
    $('.prize-carrousel').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false
    })
  })
})(jQuery)
